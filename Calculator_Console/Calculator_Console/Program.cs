﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator_Console
{
    class Program 
    {
        static void Main(string[] args)
        {
            Calculator calc = new Calculator();
            double a = 0.0, b = 0.0;
            string operation = null;
            do
            {
                Console.WriteLine("Input 1st operand");
                a = double.Parse(Console.ReadLine());

                Console.WriteLine("Input 2nd operand");
                b = double.Parse(Console.ReadLine());

                Console.WriteLine("Input operation");
                operation = Console.ReadLine().ToString();

                Console.WriteLine(calc.Calculate(operation, a, b));

                Console.WriteLine("Enter N to exit...");

            } while (Console.ReadLine().ToUpper() != "N");
        }
    }
}
