﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator_Console
{
    class Calculator
    {
        public string Calculate(string operation, double a, double b)
        {
            string result = null;
            switch (operation)
            {
                case "+":
                {
                    result = (a + b).ToString();
                    break;
                }
                case "-":
                {
                    result = (a - b).ToString();
                    break;
                }
                case "*":
                {
                    result = (a*b).ToString();
                    break;
                }
                case "/":
                {
                    if (b == 0.0)
                    {
                        Console.WriteLine("Invalid operand. Not able to divide by zero!");
                        break;
                    }
                    else result = (a/b).ToString();
                    break;
                }
            }
            return result;
        }
    }
}
